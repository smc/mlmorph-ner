import sys
from mlmorph_ner import getNamedEntities


def main():
    for line in sys.stdin:
        for entity in getNamedEntities(line):
            print(entity)

if __name__ == "__main__":
    main()
