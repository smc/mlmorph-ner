import regex
from mlmorph import Analyser

analyser = Analyser()

def quanityFinder(analyse_results):
    window=[]
    if len(window):
        yield window

def timeFinder(analyse_results):
    window=[]
    if len(window):
        yield window

def placeFinder(analyse_results):
    window=[]
    if len(window):
        yield window

def currencyFinder(analyse_results):
    window=[]
    for word in analyse_results:
        analysis = analyse_results[word]
        if not analysis:
            window=[]
            continue
        for morpheme in analysis['morphemes']:
            pos = morpheme['pos']
            if pos[0] == 'num':
                window.append(word)
            if 'cardinal' in pos:
                window.append(word)
            elif len(window):
                # FIXME, we need a currency tag
                if morpheme['root']=='രൂപ' or morpheme['root']=='പൈസ' or morpheme['root']=='ഡോളർ':
                    window.append(morpheme['root'])
                    yield window
                    window=[]

def properNameFinder(analyse_results):
    window=[]
    for word in analyse_results:
        analysis = analyse_results[word]
        if not analysis:
            window=[]
            continue
        for morpheme in analysis['morphemes']:
            pos = morpheme['pos']
            if pos[0] == 'np':
                window.append(morpheme['root'])
            elif pos[0] == 'punct':
                if len(window):
                    yield window
                window=[]
            elif len(window):
                yield window
                window=[]
    if len(window):
        yield window

def getAnalsis(word):
    weighted_analysis = analyser.analyse(word, True)
    anals_results = []
    if len(weighted_analysis) == 0:
        return False
    else:
        for aindex in range(len(weighted_analysis)):
            analysis_candidate = weighted_analysis[aindex]
            parsed_result = analyser.parse_analysis(analysis_candidate[0])
            anals_results.append(parsed_result )
        anals_results.sort(key=lambda analysis: analysis['weight'])
        return anals_results[0]

def getNamedEntities(text):
    analyse_results = {}
    for word in regex.split(r'([\s]+)', text):
        word = word.strip()
        if len(word) <= 1:
            continue
        analyse_results[word]=getAnalsis(word)

    for entity in properNameFinder(analyse_results):
        yield entity

    for entity in currencyFinder(analyse_results):
        yield entity

    for entity in timeFinder(analyse_results):
        yield entity

    for entity in quanityFinder(analyse_results):
        yield entity

    for entity in placeFinder()(analyse_results):
            yield entity

__all__ = ['getNamedEntities']
