Named Entity Recognizer based on Malayalam morphology analyser
===================================================


.. image:: https://img.shields.io/pypi/v/mlmorph-ner.svg
    :target: https://pypi.python.org/pypi/mlmorph-ner
    :alt: PyPI Version


This is a Named Entity Recognizer for Malayalam language based on `Malayalam morphology analyser - mlmorph`_.
